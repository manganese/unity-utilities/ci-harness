#!/usr/bin/env node
const path = require("path");
const fs = require("fs-extra");
const os = require("os");
const { exec } = require("child_process");
const toml = require("@iarna/toml");
const uniqBy = require("lodash/uniqBy");
const rimraf = require("rimraf");

const destroy = (path) => {
  return new Promise((resolve, reject) => {
    rimraf(path, (error) => {
      if (error) {
        reject(error);

        return;
      }

      resolve();
    });
  });
};

const PathService = require("../src/services/PathService");
const LogService = require("../src/services/LogService");
const logService = new LogService(console.log);

const yargsProjectPathPositional = (yargs) => {
  yargs.positional("project-path", {
    type: "string",
    describe: "The path of the Unity project.",
    default: ".",
  });
};

const yargsInitCommand = (yargs) => {
  return yargs.command(
    "init <project-path>",
    "Initialize a new Unity project with CI functionality.",
    (yargs) => {
      yargsProjectPathPositional(yargs);
    },
    async (argv) => {
      const bundledProjectRootPath = PathService.getBundledProjectRootPath();
      const projectRootPath = PathService.getProjectRootPath(argv.projectPath);

      await fs.copy(bundledProjectRootPath, projectRootPath);

      const projectAssetsPath = PathService.getProjectAssetsPath(
        argv.projectPath
      );
      const assetsChildren = await fs.readdir(projectAssetsPath);

      await Promise.all(
        assetsChildren.map((child) => {
          return destroy(path.join(projectAssetsPath, child));
        })
      );

      await new Promise((resolve, reject) => {
        exec(
          `chmod +x ${path.join(projectRootPath, "ci/before_script.sh")}`,
          (error) => {
            if (error) {
              reject(error);

              return;
            }

            resolve();
          }
        );
      });
    }
  );
};

const yargsInitScriptsCommand = (yargs) => {
  return yargs.command(
    "init-scripts <project-path>",
    "Initialize CI scripts in an existing Unity project.",
    (yargs) => {
      yargsProjectPathPositional(yargs);
    },
    async (argv) => {
      const bundledProjectScriptsPath = PathService.getProjectScriptsPath(
        PathService.getBundledProjectRootPath()
      );
      const projectScriptsPath = PathService.getProjectScriptsPath(
        argv.projectPath
      );

      await fs.copy(bundledProjectScriptsPath, projectScriptsPath);
    }
  );
};

const yargsInitBuildCommand = (yargs) => {
  return yargs.command(
    "init-build <project-path>",
    "Initialize build scripts within the Assets directory of an existing Unity project.",
    (yargs) => {
      yargsProjectPathPositional(yargs);
    },
    async (argv) => {
      const bundledProjectBuildScriptsPath =
        PathService.getProjectBuildScriptsPath(
          PathService.getBundledProjectRootPath()
        );
      const projectBuildScriptsPath = PathService.getProjectBuildScriptsPath(
        argv.projectPath
      );

      await fs.copy(bundledProjectBuildScriptsPath, projectBuildScriptsPath);
    }
  );
};

const yargsAddDependenciesCommand = (yargs) => {
  return yargs.command(
    "add-dependencies <project-path> <source-file>",
    "Add dependencies and scoped registries to a Unity project.",
    (yargs) => {
      yargsProjectPathPositional(yargs);

      yargs.positional("source-file", {
        type: "string",
        describe:
          "The package.json file containing any dependencies or scoped registries to add.",
      });
    },
    async (argv) => {
      const projectPackagesManifestPath =
        PathService.getProjectPackagesManifestPath(argv.projectPath);
      const projectPackagesManifest = await fs.readJson(
        projectPackagesManifestPath
      );

      const sourcePackage = await fs.readJson(argv.sourceFile);

      await fs.writeJson(
        projectPackagesManifestPath,
        {
          ...projectPackagesManifest,
          scopedRegistries: uniqBy(
            (projectPackagesManifest.scopedRegistries || []).concat(
              sourcePackage.scopedRegistries || []
            ),
            (scopedRegistry) => scopedRegistry.name
          ),
          dependencies: {
            ...projectPackagesManifest.dependencies,
            ...sourcePackage.dependencies,
          },
        },
        {
          spaces: 2,
        }
      );
    }
  );
};

const yargsAuthRegistryCommand = (yargs) => {
  return yargs.command(
    "auth-registry",
    "Add an authentication token for a UPM scoped registry.",
    (yargs) => {
      yargsProjectPathPositional(yargs);

      yargs.option("registry", {
        type: "string",
        describe: "The URL of the registry.",
        requiresArg: true,
      });

      yargs.option("token", {
        type: "string",
        describe: "The token to use to authenticate with the registry.",
        requiresArg: true,
      });
    },
    async (argv) => {
      const upmConfigPath = path.join(os.homedir(), ".upmconfig.toml");
      let upmConfig = {};

      try {
        const upmConfigContents = await fs.readFile(upmConfigPath);

        upmConfig = toml.parse(upmConfigContents.toString());
      } catch (error) {
        logService.warn("Could not read UPM configuration file.");
      }

      if (!upmConfig.npmAuth) {
        upmConfig.npmAuth = {};
      }

      if (upmConfig.npmAuth.hasOwnProperty(argv.registry)) {
        logService.warn(
          "An authentication entry for the given registry already exists, it will be overwritten."
        );
      }

      upmConfig.npmAuth[argv.registry] = {
        token: argv.token,
        alwaysAuth: true,
      };

      await fs.writeFile(upmConfigPath, toml.stringify(upmConfig));
    }
  );
};

const yargsExpandCommand = (yargs) => {
  return yargs.command(
    "expand <project-path>",
    "Move the contents of a Unity project to the current directory.",
    (yargs) => {
      yargsProjectPathPositional(yargs);

      yargs.option("subdirectory", {
        type: "string",
        describe:
          "The subdirectory within the Assets folder to move the contents of the current directory into.",
        requiresArg: true,
      });
    },
    async (argv) => {
      const projectRootPath = PathService.getProjectRootPath(argv.projectPath);
      const projectAssetsPath = PathService.getProjectAssetsPath(
        argv.projectPath
      );
      const destinationPath = path.join(
        projectAssetsPath,
        argv.subdirectory || ""
      );

      await fs.ensureDir(destinationPath);

      const children = await fs.readdir(".");

      await Promise.all(
        children.map((child) => {
          if (child === projectRootPath) return Promise.resolve();

          return fs.move(child, path.join(destinationPath, child));
        })
      );

      const projectChildren = await fs.readdir(projectRootPath);

      await Promise.all(
        projectChildren.map((child) => {
          return fs.move(
            path.join(projectRootPath, child),
            path.join(".", child)
          );
        })
      );

      await destroy(projectRootPath);
    }
  );
};

const yargsFinalizeCommand = (yargs) => {
  return yargs.command(
    "finalize",
    "Execute the bundled script to prepare the Unity project once the files are in place.",
    (yargs) => {},
    async (argv) => {
      const projectRootPath = ".";
      const beforeScriptPath = path.join(
        projectRootPath,
        "ci/before_script.sh"
      );

      await new Promise((resolve, reject) => {
        exec(`./${beforeScriptPath}`, (error) => {
          if (error) {
            reject(error);

            return;
          }

          resolve();
        });
      });
    }
  );
};

const yargsTestCommand = (yargs) => {
  return yargs.command(
    "test",
    "Execute the bundled test script.",
    (yargs) => {},
    async (argv) => {
      const projectRootPath = ".";
      const testScriptPath = path.join(projectRootPath, "ci/test.sh");

      await new Promise((resolve, reject) => {
        exec(`chmod +x ${testScriptPath} && ./${testScriptPath}`, (error) => {
          if (error) {
            reject(error);

            return;
          }

          resolve();
        });
      });
    }
  );
};

const yargs = require("yargs")
  .scriptName("unity-ci-harness")
  .usage("$0 <command> [arguments]");

yargsInitCommand(yargs);
yargsInitScriptsCommand(yargs);
yargsInitBuildCommand(yargs);
yargsAddDependenciesCommand(yargs);
yargsAuthRegistryCommand(yargs);
yargsExpandCommand(yargs);
yargsFinalizeCommand(yargs);
yargsTestCommand(yargs);

yargs.help().argv;
