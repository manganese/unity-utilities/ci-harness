const path = require("path");

module.exports = class PathService {
  static getBundledProjectRootPath() {
    return path.join(__dirname, "../../project");
  }

  static getProjectRootPath(projectPath) {
    return projectPath;
  }

  static getProjectScriptsPath(projectPath) {
    return path.join(projectPath, "ci");
  }

  static getProjectBuildScriptsPath(projectPath) {
    return path.join(projectPath, "Assets/Scripts");
  }

  static getProjectAssetsPath(projectPath) {
    return path.join(projectPath, "Assets");
  }

  static getProjectPackagesManifestPath(projectPath) {
    return path.join(projectPath, "Packages/manifest.json");
  }
};
